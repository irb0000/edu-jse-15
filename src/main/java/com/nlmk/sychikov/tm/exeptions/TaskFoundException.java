package com.nlmk.sychikov.tm.exeptions;

public class TaskFoundException extends Throwable{
    public TaskFoundException(String message) {
        super(message);
    }
}
