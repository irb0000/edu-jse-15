package com.nlmk.sychikov.tm.controller;

import com.nlmk.sychikov.tm.entity.Task;
import com.nlmk.sychikov.tm.enumerated.RoleType;
import com.nlmk.sychikov.tm.exeptions.ProjectFoundException;
import com.nlmk.sychikov.tm.exeptions.TaskFoundException;
import com.nlmk.sychikov.tm.service.ProjectTaskService;
import com.nlmk.sychikov.tm.service.TaskService;

import java.util.*;

public class TaskController extends AbstractController {

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    private final UserController userController;

    public TaskController(
            final TaskService taskService, final ProjectTaskService projectTaskService,
            final UserController userController
    ) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.userController = userController;
    }

    /**
     * Add new task to user
     *
     * @return return value
     */
    public int createTask(Long userId) {
        System.out.println("[CREATE TASK]");
        if (!userController.checkCurrentUserAdminPermissionAlarm()) return -1;
        System.out.print("Enter task name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter task description: ");
        final String description = scanner.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) {
            System.out.println("[FAILED]");
            return -1;
        }
        task.setUserId(userId);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Assign project to user
     *
     * @return return value
     */
    public int assignTaskToUserByIds() throws TaskFoundException {
        System.out.println("[ASSIGN TASK TO USER BY IDS]");
        final Long taskId = longInputProcessor("Enter task id: ");
        if (taskId == -1)
            return -1;
        if (!checkUserPermission(taskId)) return -1;
        final Long userId = longInputProcessor("Enter user id: ");
        if (userId == -1)
            return -1;
        final Task task;
        try {
            task = taskService.assignToUser(taskService.findById(taskId), userId);
        } catch (TaskFoundException e) {
            System.out.println("[FAILED]");
            throw e;
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Create new task to current user
     *
     * @return return value
     */
    public int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter task name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter task description: ");
        final String description = scanner.nextLine();
        if (taskService.create(name, description, userController.getCurrentUserId()) == null) {
            System.out.println("[FAILED]");
            return -1;
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Update task by index
     *
     * @return return value
     */
    public int updateTaskByIndex() throws TaskFoundException {
        System.out.println("[UPDATE TASK BY INDEX]");
        final int index = intInputProcessor("Enter task index: ");
        if (index == -1)
            return 0;
        final Task task = taskService.findByIndex(index);
        if (!checkUserPermission(task.getId())) return -1;
        System.out.print("Enter task name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter task description: ");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Update task by id
     *
     * @return return value
     */
    public int updateTaskById() throws TaskFoundException {
        System.out.println("[UPDATE TASK BY ID]");
        final Long id = longInputProcessor("Enter task id: ");
        if (id == -1)
            return 0;
        final Task task;
            task = taskService.findById(id);
        if (!checkUserPermission(task.getId())) return -1;
        System.out.print("Enter task name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter task description: ");
        final String description = scanner.nextLine();
        taskService.update(id, name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes task by id
     *
     * @return return value
     */
    public int removeTaskById() throws TaskFoundException {
        System.out.println("[REMOVE TASK BY ID]");
        final Long id = longInputProcessor("Enter task id: ");
        if (id == -1)
            return 0;
        final Task task = taskService.findById(id);
        if (!checkUserPermission(task.getId())) return -1;
        taskService.removeById(id);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes task by index
     *
     * @return return value
     */
    public int removeTaskByIndex() throws TaskFoundException {
        System.out.println("[REMOVE TASK BY INDEX]");
        final int index = intInputProcessor("Enter task index: ");
        if (index == -1)
            return 0;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[Task with index=" + (index + 1) + " is not found!]");
            return -1;
        }
        if (!checkUserPermission(task.getId())) return -1;
        taskService.removeByIndex(index);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Clear all tasks
     *
     * @return return value
     */
    public int clearTask() {
        System.out.println("[CLEAR TASKS]");
        if (!userController.checkCurrentUserAdminPermissionAlarm()) return -1;
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Clear all tasks for current user
     *
     * @return return value
     */
    public int clearCurrentUserTask() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear(userController.getCurrentUserId());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Show the task
     *
     * @param task what task should be printed
     */
    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASKS]");
        System.out.println("ID:" + task.getId());
        System.out.println("NAME:" + task.getName());
        System.out.println("DESCRIPTION:" + task.getDescription());
        System.out.println("[OK]");
    }

    /**
     * Shows task by index
     *
     * @return return value
     */
    public int viewTaskByIndex() throws TaskFoundException {
        final int index = intInputProcessor("Enter task index: ");
        if (index == -1)
            return -1;
        final Task task = taskService.findByIndex(index);
        if (!checkUserPermission(task.getId())) return -1;
        viewTask(task);
        return 0;
    }

    /**
     * Shows task by id
     *
     * @return return value
     */
    public int viewTaskById() throws TaskFoundException {
        final Long id = longInputProcessor("Enter task id: ");
        if (id == -1)
            return 0;
        final Task task = taskService.findById(id);
        if (!checkUserPermission(task.getId())) return -1;
        viewTask(task);
        return 0;
    }

    /**
     * Render tasks from list
     *
     * @param tasks task list
     */
    public void renderTasks(final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        int index = 1;
        Collections.sort(tasks, Comparator.comparing(n->n.getName()));
        for (Task task : tasks) {
            System.out.println(
                    index + ". UID=" + task.getUserId() + ": " +
                            task.getId() + ": " +
                            task.getName() + ": " + task.getDescription()
            );
            index++;
        }
    }

    /**
     * List all tasks
     *
     * @return return value
     */
    public int listAllTask() {
        System.out.println("[LIST TASKS]");
        if (!userController.checkCurrentUserAdminPermissionAlarm()) return -1;
        renderTasks(taskService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * List all tasks for current user
     *
     * @return return value
     */
    public int listTask() throws TaskFoundException {
        System.out.println("[LIST TASKS]");
        renderTasks(taskService.findAllByUserId(userController.getCurrentUserId()));
        System.out.println("[OK]");
        return 0;
    }

    /**
     * List all tasks for user
     *
     * @return return value
     */
    public int listUserTask() throws TaskFoundException {
        System.out.println("[LIST TASKS]");
        if (!userController.checkCurrentUserAdminPermissionAlarm()) return -1;
        final Long userId = longInputProcessor("Enter user id: ");
        if (userId == -1)
            return -1;
        renderTasks(taskService.findAllByUserId(userId));
        System.out.println("[OK]");
        return 0;
    }

    /**
     * List project tasks by projectId
     *
     * @return return value
     */
    public int listTaskByProjectId() throws ProjectFoundException {
        System.out.println("[LIST TASK FROM PROJECT BY ID]");
        final Long projectId = longInputProcessor("Enter project id: ");
        if (projectId == -1)
            return -1;
        if (userController.checkCurrentUserAdminPermission())
            renderTasks(projectTaskService.findAllByProjectId(projectId));
        else
            renderTasks(
                    projectTaskService.findAllByProjectIdAndUserId(projectId, userController.getCurrentUserId())
            );
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Add task to project
     *
     * @return return value
     */
    public int addTaskToProjectByIds() {
        System.out.println("[ADD TASK TO PROJECT BY IDS]");
        final Long projectId = longInputProcessor("Enter project id: ");
        if (projectId == -1)
            return 0;
        final Long taskId = longInputProcessor("Enter task id: ");
        if (taskId == -1)
            return 0;
        projectTaskService.addTaskToProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Remove task from project by projectId and taskId
     *
     * @return return value
     */
    public int removeTaskFromProjectByIds() {
        System.out.println("[REMOVE TASK FROM PROJECT BY IDS]");
        final Long projectId = longInputProcessor("Enter project id: ");
        if (projectId == -1)
            return 0;
        final Long taskId = longInputProcessor("Enter task id: ");
        if (taskId == -1)
            return 0;
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Check does the user have ADMIN role or user is task owner
     * and print alarm if hasn't.
     *
     * @return true if has ADMIN role or user is current user.
     */
    private boolean checkUserPermission(final Long taskId) {
        if (taskId == null) return false;
        final Task task;
        try {
            task = taskService.findById(taskId);
        } catch (TaskFoundException e) {
            return false;
        }
        //if (task == null) return false;
        if (userController.getCurrentUser().getId().equals(task.getUserId())) return true;
        return userController.checkCurrentUserAdminPermissionAlarm();
    }

}
