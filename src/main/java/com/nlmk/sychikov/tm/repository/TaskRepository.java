package com.nlmk.sychikov.tm.repository;

import com.nlmk.sychikov.tm.entity.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Tasks storage
 */
public class TaskRepository {

    private final List<Task> tasks = new ArrayList<>();
    private final Map<String, List<Task>> nameIndex = new HashMap<>();

    private void addToRepo(final Task task) {
        addToNameIndex(task);
        tasks.add(task);
    }

    private List<Task> findInNameIndex(Task task) {
        return nameIndex.get(task.getName());
    }

    private void addToNameIndex(Task task) {
        nameIndex.computeIfAbsent(task.getName(), k -> new ArrayList<Task>()).add(task);
    }

    private Task removeFromNameIndex(Task task) {
        final List<Task> taskList = findInNameIndex(task);
        if (taskList != null) taskList.remove(task);
        if (taskList.isEmpty()) nameIndex.remove(task.getName());
        return task;
    }

    private void removeTask(Task task) {
        removeFromNameIndex(task);
        tasks.remove(task);
    }

    public Task create(final String name) {
        final Task task = new Task(name);
        addToRepo(task);
        return task;
    }

    public Task create(final String name, final String description) {
        final Task task = new Task(name, description);
        addToRepo(task);
        return task;
    }

    public Task create(final String name, final String description, final Long userId) {
        final Task task = new Task(name, description);
        task.setUserId(userId);
        addToRepo(task);
        return task;
    }

    public int getRepositorySize() {
        return tasks.size();
    }

    public Task assignToUser(final Task task, final Long userId) {
        task.setUserId(userId);
        return task;
    }

    public Task update(final Long id, final String name, final String description) {
        final Task task = findById(id);
        if (task == null) return null;
        removeFromNameIndex(task);
        task.setName(name);
        task.setDescription(description);
        addToNameIndex(task);
        return task;
    }

    public void clear() {
        nameIndex.clear();
        tasks.clear();
    }

    public void clear(final Long userId) {
        final List<Task> userTasks = findAllByUserId(userId);
        if (userTasks == null) return;
        for (final Task task : userTasks) {
            removeTask(task);
        }
    }

    public Task findByIndex(final int index) {
        return tasks.get(index);
    }

    public Task findById(final Long id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    public Task findById(final Long id, final Long userId) {
        for (final Task task : tasks) {
            if (id.equals(task.getId()) && userId.equals(task.getUserId())) return task;
        }
        return null;
    }

    public List<Task> findByName(final String name) {
        return  nameIndex.get(name);
    }

    public List<Task> findByName(final String name, final Long userId) {
        final List<Task> taskList = new ArrayList<>();
        for (final Task task : nameIndex.get(name)) {
            if (userId.equals(task.getUserId())) taskList.add(task);
        }
        return taskList;
    }

    public Task removeById(final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeById(final Long id, final Long userId) {
        final Task task = findById(id, userId);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

//    public Task removeByName(final String name) {
//        final Task task = findByName(name);
//        if (task == null) return null;
//        tasks.remove(task);
//        return task;
//    }
//
//    public Task removeByName(final String name, final Long userId) {
//        final Task task = findByName(name, userId);
//        if (task == null) return null;
//        tasks.remove(task);
//        return task;
//    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        final Long idProject = task.getProjectId();
        if (idProject == null) return null;
        if (idProject.equals(projectId)) return task;
        return null;
    }

    public List<Task> findAll() {
        return tasks;
    }

    public List<Task> findAllByUserId(final Long userId) {
        List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            Long idUser = task.getUserId();
            if (idUser == null) continue;
            if (idUser.equals(userId)) result.add(task);
        }
        return result;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            Long Idproject = task.getProjectId();
            if (Idproject == null) continue;
            if (projectId.equals(Idproject)) result.add(task);
        }
        return result;
    }

    public Task removeFromProjectByIds(final Long projectId, final Long id) {
        final Task task = findByProjectIdAndId(projectId, id);
        if (task == null) return null;
        task.setProjectId(null);
        return task;

    }

}
