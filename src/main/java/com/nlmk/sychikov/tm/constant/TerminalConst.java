package com.nlmk.sychikov.tm.constant;

public class TerminalConst {

    public static final String HELP = "help";
    public static final String VERSION = "version";
    public static final String ABOUT = "about";
    public static final String EXIT = "exit";
    public static final String DISPLAY_LAST_COMMAND = "last-command";

    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_CURRENT_CLEAR = "project-clear-current";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_CURRENT_LIST = "project-list-current";
    public static final String PROJECT_USER_LIST = "project-list-user";
    public static final String PROJECT_VIEW_BY_INDEX = "project-view-by-index";
    public static final String PROJECT_VIEW_BY_ID = "project-view-by-id";
    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";
    public static final String PROJECT_UPDATE_BY_ID = "project-update-by-id";
    public static final String PROJECT_ASSIGN_TO_USER_BY_ID = "project-assign-to-user-by-id";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_CURRENT_CLEAR = "task-clear-current";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_CURRENT_LIST = "task-list-current";
    public static final String TASK_USER_LIST = "task-list-user";
    public static final String TASK_VIEW_BY_INDEX = "task-view-by-index";
    public static final String TASK_VIEW_BY_ID = "task-view-by-id";
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";
    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";
    public static final String TASK_ASSIGN_TO_USER_BY_ID = "task-assign-to-user-by-id";
    public static final String TASK_LIST_BY_PROJECT_ID = "task-list-by-project-id";
    public static final String TASK_ADD_TO_PROJECT_BY_IDS = "task-add-to-project-by-ids";
    public static final String TASK_REMOVE_FROM_PROJECT_BY_IDS = "task-remove-from-project-by-ids";

    public static final String USER_CREATE = "user-create";
    public static final String USER_CLEAR = "user-clear";
    public static final String USER_LIST = "user-list";
    public static final String USER_UPDATE_BY_LOGIN = "user-update-by-login";
    public static final String USER_UPDATE_BY_ID = "user-update-by-ID";
    public static final String USER_VIEW_BY_LOGIN = "user-view-by-login";
    public static final String USER_VIEW_BY_ID = "user-view-by-id";
    public static final String USER_REMOVE_BY_ID = "user-remove-by-id";
    public static final String USER_REMOVE_BY_LOGIN = "user-remove-by-login";
    public static final String USER_CHANGE_CURRENT_PASSWORD = "passwd";
    public static final String USER_CHANGE_PASSWORD = "user-passwd";
    public static final String USER_VIEW_CURRENT_PROFILE = "user-view-profile";
    public static final String USER_LOGIN = "login";
    public static final String USER_LOGOUT = "logout";
    public static final String USER_UPDATE_CURRENT_PROFILE = "user-update-profile";

    public static final Integer MAX_COMMAND_HISTORY_SIZE = 10;

}
