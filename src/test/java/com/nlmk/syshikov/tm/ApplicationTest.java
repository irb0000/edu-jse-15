package com.nlmk.syshikov.tm;

import static org.junit.Assert.assertTrue;

import com.nlmk.sychikov.tm.Application;
import com.nlmk.sychikov.tm.entity.Project;
import com.nlmk.sychikov.tm.entity.Task;
import com.nlmk.sychikov.tm.exeptions.ProjectFoundException;
import com.nlmk.sychikov.tm.exeptions.TaskFoundException;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class ApplicationTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        final Application application = new Application();
        Task task = null;
        try {
            task = application.getTaskService().findByIndex(0);
        } catch (TaskFoundException e) {
            e.printStackTrace();
        }
        System.out.println(task);
        Project project = null;
        try {
            project = application.getProjectService().findByIndex(0);
        } catch (ProjectFoundException e) {
            e.printStackTrace();
        }
        System.out.println(project);
        System.out.println("Adding task...");
        application.getProjectTaskService().addTaskToProject(project.getId(),task.getId());
        try {
            application.getProjectTaskService().addTaskToProject(project.getId(),application.getTaskService().findByIndex(1).getId());
        } catch (TaskFoundException e) {
            e.printStackTrace();
        }
        try {
            application.getTaskController().renderTasks(application.getProjectTaskService().findAllByProjectId(project.getId()));
        } catch (ProjectFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Removing one task...");
        application.getProjectTaskService().removeTaskFromProject(project.getId(),task.getId());
        try {
            application.getTaskController().renderTasks(application.getProjectTaskService().findAllByProjectId(project.getId()));
        } catch (ProjectFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Removing project...");
        try {
            application.getProjectTaskService().removeProjectByIndex(0);
        } catch (ProjectFoundException e) {
            e.printStackTrace();
        }
        System.out.println(task);

        assertTrue(true);
    }
}
