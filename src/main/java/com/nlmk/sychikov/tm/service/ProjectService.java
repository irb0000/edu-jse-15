package com.nlmk.sychikov.tm.service;

import com.nlmk.sychikov.tm.entity.Project;
import com.nlmk.sychikov.tm.exeptions.ProjectFoundException;
import com.nlmk.sychikov.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public Project create(final String name, final String description) {
        if (description == null || description.isEmpty()) return create(name);
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    public Project create(final String name, final String description, final Long userId) {
        if (userId == null) return create(name, description);
        if (description == null || description.isEmpty()) return create(name);
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name, description, userId);
    }

    public Project update(final Long id, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (id == null) return null;
        return projectRepository.update(id, name, description);
    }

    public void clear() {
        projectRepository.clear();
    }

    public Project findByIndex(final int index) throws ProjectFoundException {
        if (index > projectRepository.getRepositorySize() - 1 || index < 0)
            throw new ProjectFoundException("Project index=".concat(String.valueOf(index)).concat(" out of bounds!"));
        return projectRepository.findByIndex(index);
    }

    public Project assignToUser(Long projectId, Long userId) throws ProjectFoundException {
        if (projectId == null) return null;
        if (userId == null) return null;
        final Project project = projectRepository.findById(projectId);
        if (project == null) throw new ProjectFoundException("Project id=".concat(projectId.toString()).concat(" isn't found!"));
        return projectRepository.assignToUser(project, userId);
    }

    public Project findById(final Long id) throws ProjectFoundException {
        if (id == null) return null;
        final Project project = projectRepository.findById(id);
        if (project== null) throw new ProjectFoundException("Project isn't found by id=".concat(id.toString()));
        return project;
    }

    public Project findById(final Long id, final Long userId) {
        if (userId == null) return null;
        if (id == null) return null;
        return projectRepository.findById(id, userId);
    }

    public List<Project> findAll() throws ProjectFoundException {
        List<Project> projectList = projectRepository.findAll();
        if (projectList.isEmpty()) throw new ProjectFoundException("Project list is empty. At all.");
        return projectList;
    }

    public List<Project> findAllByUserId(Long userId) throws ProjectFoundException {
        if (userId == null) return null;
        final List<Project> projectList = projectRepository.findAllByUserId(userId);
        if(projectList.isEmpty()) throw new ProjectFoundException("User userId=".concat(userId.toString()).concat(" hasn't projects yet."));
        return projectList;
    }

    public List<Project> findByName(final String name) throws ProjectFoundException {
        if (name == null || name.isEmpty()) return null;
        List<Project> projectList = projectRepository.findByName(name);
        if(projectList.isEmpty()) throw new ProjectFoundException("Projects with name=".concat(name).concat(" are not found."));
        return projectList;
    }

}
