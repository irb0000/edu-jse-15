package com.nlmk.sychikov.tm.service;

import com.nlmk.sychikov.tm.entity.Task;
import com.nlmk.sychikov.tm.exeptions.TaskFoundException;
import com.nlmk.sychikov.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name);
    }

    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description);
    }

    public Task create(final String name, final String description, final Long userId) {
        if (userId == null) return create(name, description);
        if (description == null || description.isEmpty()) return create(name);
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name, description, userId);
    }

    public Task update(final Long id, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (id == null) return null;
        return taskRepository.update(id, name, description);
    }

    public void clear() {
        taskRepository.clear();
    }

    public void clear(final Long userId) {
        if (userId == null) return;
        taskRepository.clear(userId);
    }

    public Task findByIndex(final int index) throws TaskFoundException {
        if (index > taskRepository.getRepositorySize() - 1 || index < 0)
            throw new TaskFoundException("Task isn't found. Index=".concat(String.valueOf(index)).concat(" out of bounds!"));
        final Task task = taskRepository.findByIndex(index);
        if (task == null) throw new TaskFoundException("Task with index=".concat(String.valueOf(index)).concat(" isn't found!"));
        return task;
    }

    public Task assignToUser(Task task, Long userId) {
        if (task == null) return null;
        if (userId == null) return null;
        return taskRepository.assignToUser(task, userId);
    }

    public Task findById(final Long id) throws TaskFoundException {
        if (id == null) throw new TaskFoundException("Task id is null!");
        Task task = taskRepository.findById(id);
        if (task == null) throw new TaskFoundException("Task isn't found for taskId=".concat(id.toString()));
        return task;
    }

    public Task findById(final Long id, final Long userId) {
        if (userId == null) return null;
        if (id == null) return null;
        return taskRepository.findById(id, userId);
    }

    public List<Task> findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    public Task removeById(final Long id) throws TaskFoundException {
        if (id == null) throw new TaskFoundException("Task id is null! Can't perform task searching.");
        Task task = taskRepository.removeById(id);
        if (task == null)
            throw new TaskFoundException("Task isn't removed! It isn't found for taskId=".concat(id.toString()));
        return task;
    }

    public Task removeById(final Long id, final Long userId) {
        if (userId == null) return null;
        if (id == null) return null;
        return taskRepository.removeById(id);
    }

    public Task removeByIndex(final int index) throws TaskFoundException {
        if (index > taskRepository.getRepositorySize() - 1 || index < 0)
            throw new TaskFoundException("Wrong task index!");
        ;
        Task task = taskRepository.removeByIndex(index);
        if (task == null)
            throw new TaskFoundException("Task isn't removed! It isn't found for taskIndex=".concat(String.valueOf(index)));
        return task;
    }

//    public Task removeByName(final String name) {
//        if (name == null || name.isEmpty()) return null;
//        return taskRepository.removeByName(name);
//    }

//    public Task removeByName(final String name, final Long userId) {
//        if (name == null || name.isEmpty()) return null;
//        if (userId == null) return null;
//        return taskRepository.removeByName(name, userId);
//    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public int getRepositorySize() {
        return taskRepository.getRepositorySize();
    }

    public List<Task> findAllByUserId(Long userId) throws TaskFoundException {
        if (userId == null)  throw new TaskFoundException("Wrong userId! UserId must be not null.");
        List<Task> taskList = taskRepository.findAllByUserId(userId);
        if (taskList.isEmpty()) throw new TaskFoundException("User userId=".concat(userId.toString()).concat(" hasn't tasks yet."));
        return taskList;
    }

}
