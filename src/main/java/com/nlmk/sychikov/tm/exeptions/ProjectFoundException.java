package com.nlmk.sychikov.tm.exeptions;

public class ProjectFoundException extends Throwable{
    public ProjectFoundException(String message) {
        super(message);
    }
}
