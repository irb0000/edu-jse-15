package com.nlmk.sychikov.tm.controller;

import com.nlmk.sychikov.tm.entity.Project;
import com.nlmk.sychikov.tm.exeptions.ProjectFoundException;
import com.nlmk.sychikov.tm.service.ProjectService;
import com.nlmk.sychikov.tm.service.ProjectTaskService;

import java.util.Comparator;
import java.util.List;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    private final ProjectTaskService projectTaskService;

    private final UserController userController;

    public ProjectController(
            final ProjectService projectService, final ProjectTaskService projectTaskService,
            final UserController userController
    ) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
        this.userController = userController;
    }

    /**
     * Create new project to user
     *
     * @param userId project will be created for user with userId
     * @return return value
     */
    public int createProject(Long userId) {
        System.out.println("[CREATE PROJECT]");
        if (!userController.checkCurrentUserAdminPermissionAlarm()) return -1;
        if (userId == null) return -1;
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter project description: ");
        final String description = scanner.nextLine();
        if (projectService.create(name, description, userId) == null) {
            System.out.println("[FAILED]");
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Assign project to user
     *
     * @return return value
     */
    public int assignProjectToUserByIds() throws ProjectFoundException {
        System.out.println("[ASSIGN PROJECT TO USER BY IDS]");
        final Long projectId = longInputProcessor("Enter project id: ");
        if (projectId == -1)
            return -1;
        final Long userId = longInputProcessor("Enter user id: ");
        if (userId == -1)
            return -1;
        if (!checkUserOrAdminPermission(projectId)) return -1;
        final Project project = projectService.assignToUser(projectId, userId);
        if (project == null) {
            System.out.println("[FAILED]");
            return -1;
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Add new project to current user
     *
     * @return return value
     */
    public int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter project description: ");
        final String description = scanner.nextLine();
        if (projectService.create(name, description, userController.getCurrentUserId()) == null) {
            System.out.println("[FAILED]");
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Update project by id
     *
     * @return return value
     */
    public int updateProjectById() throws ProjectFoundException {
        System.out.println("[UPDATE PROJECT BY ID]");
        final Long id = longInputProcessor("Enter project id: ");
        if (id == -1)
            return 0;
        final Project project = projectService.findById(id);
        if (!checkUserOrAdminPermission(project.getId())) return -1;
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter project description: ");
        final String description = scanner.nextLine();
        projectService.update(id, name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Update project by index
     *
     * @return return value
     */
    public int updateProjectByIndex() throws ProjectFoundException {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        final int index = intInputProcessor("Enter project index: ");
        if (index == -1)
            return 0;
        final Project project = projectService.findByIndex(index);
        if (!checkUserOrAdminPermission(project.getId())) return -1;
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter project description: ");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes project by name
     *
     * @return return value
     */
    public int removeProjectByName() throws ProjectFoundException {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        final List<Project> projectList = projectService.findByName(name);
        for (Project project : projectList) {
            if (!checkUserOrAdminPermission(project.getId())) continue;
            projectTaskService.removeProject(project);
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes project by id
     *
     * @return return value
     */
    public int removeProjectById() throws ProjectFoundException {
        System.out.println("[REMOVE PROJECT BY ID]");
        final Long id = longInputProcessor("Enter project id: ");
        if (id == -1)
            return -1;
        final Project project = projectService.findById(id);
        if (!checkUserOrAdminPermission(project.getId())) return -1;
        projectTaskService.removeProjectById(project.getId());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes project by index
     *
     * @return return value
     */
    public int removeProjectByIndex() throws ProjectFoundException {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        final int index = intInputProcessor("Enter project index: ");
        if (index == -1)
            return -1;
        Project project = projectService.findByIndex(index);
        if (!checkUserOrAdminPermission(project.getId())) return -1;
        projectTaskService.removeProjectByIndex(index);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Clear all projects
     *
     * @return return value
     */
    public int clearProject() {
        System.out.println("[CLEAR PROJECTS]");
        if (!userController.checkCurrentUserAdminPermissionAlarm()) return -1;
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * clear all project by user id
     *
     * @return return value
     */
    public int clearCurrentUserProjects() throws ProjectFoundException {
        System.out.println("[CLEAR PROJECTS]");
        projectTaskService.removeProjectByUserId(userController.getCurrentUserId());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Shows  projects list ordered by name
     *
     * @param projects projects list
     */
    private void renderProject(List<Project> projects) {
        projects.sort(Comparator.comparing(Project::getName));
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". UID=" + project.getUserId() + ": " +
                    project.getId() + ": " + project.getName() + ": " + project.getDescription());
            index++;
        }

    }

    /**
     * List all projects
     *
     * @return return value
     */
    public int listAllProject() throws ProjectFoundException {
        System.out.println("[LIST ALL PROJECTS]");
        if (!userController.checkCurrentUserAdminPermissionAlarm()) return -1;
        renderProject(projectService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * List all projects by current user
     *
     * @return return value
     */
    public int listProject() throws ProjectFoundException {
        System.out.println("[LIST PROJECTS]");
        final List<Project> projects = projectService.findAllByUserId(userController.getCurrentUserId());
        renderProject(projects);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * List all projects by user id
     *
     * @return return value
     */
    public int listUserProject() throws ProjectFoundException {
        System.out.println("[LIST PROJECTS]");
        if (!userController.checkCurrentUserAdminPermissionAlarm()) return -1;
        final Long userId = longInputProcessor("Enter user id: ");
        if (userId == -1)
            return -1;
        final List<Project> projects = projectService.findAllByUserId(userId);
        renderProject(projects);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Show the project
     *
     * @param project what project should be printed
     */
    public void viewProject(final Project project) {
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID:" + project.getId());
        System.out.println("NAME:" + project.getName());
        System.out.println("DESCRIPTION:" + project.getDescription());
        System.out.println("[OK]");
    }

    /**
     * Shows project by index
     *
     * @return return value
     */
    public int viewProjectByIndex() throws ProjectFoundException {
        final int index = intInputProcessor("Enter project index: ");
        if (index == -1)
            return -1;
        final Project project = projectService.findByIndex(index);
        if (!checkUserOrAdminPermission(project.getId())) return -1;
        viewProject(project);
        return 0;
    }

    /**
     * Shows project by id
     *
     * @return return value
     */
    public int viewProjectById() throws ProjectFoundException {
        final Long id = longInputProcessor("Enter project id: ");
        if (id == -1)
            return 0;
        final Project project = projectService.findById(id);
        if (!checkUserOrAdminPermission(project.getId())) return -1;
        viewProject(project);
        return 0;
    }

    /**
     * Check does the user have ADMIN role or user is project owner
     * and print alarm if hasn't.
     *
     * @param projectId project identifier
     * @return true if has ADMIN role or user is current user.
     */
    private boolean checkUserOrAdminPermission(Long projectId) throws ProjectFoundException {
        if (projectId == null) return false;
        final Project project;
            project = projectService.findById(projectId);
        if (userController.getCurrentUser().getId().equals(project.getUserId())) return true;
        return userController.checkCurrentUserAdminPermissionAlarm();
    }

    /**
     * Check does the user is project owner
     * and print alarm if hasn't.
     *
     * @return true if has ADMIN role or user is current user.
     */
    private boolean checkUserPermission(Project project) {
        if (project == null) return false;
        if (userController.getCurrentUser().getId().equals(project.getUserId())) return true;
        return userController.checkCurrentUserAdminPermissionAlarm();
    }

}
