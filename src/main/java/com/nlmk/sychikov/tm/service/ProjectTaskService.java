package com.nlmk.sychikov.tm.service;

import com.nlmk.sychikov.tm.entity.Project;
import com.nlmk.sychikov.tm.entity.Task;
import com.nlmk.sychikov.tm.exeptions.ProjectFoundException;
import com.nlmk.sychikov.tm.repository.ProjectRepository;
import com.nlmk.sychikov.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {

    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;

    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public List<Task> findAllByProjectId(final Long projectId) throws ProjectFoundException {
        if (projectId == null) return Collections.emptyList();
        List<Task> taskList = taskRepository.findAllByProjectId(projectId);
        if (taskList.isEmpty()) throw new ProjectFoundException("Tasks aren't found for projectId=".concat(projectId.toString()));
        return taskList;
    }

    public List<Task> findAllByProjectIdAndUserId(final Long projectId, final Long userId) throws ProjectFoundException {
        if (projectId == null) throw new ProjectFoundException("Project id must be not null!");//return Collections.emptyList();
        if (userId == null) throw new ProjectFoundException("User id must be not null!");//return Collections.emptyList();
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        if (tasks == null) throw new ProjectFoundException("Project id=".concat(projectId.toString()).concat(" hasn't tasks yet!"));
        tasks.removeIf(task -> !task.getUserId().equals(userId));
        if (tasks.isEmpty()) throw new ProjectFoundException("Project id=".concat(projectId.toString()).concat(" hasn't tasks yet!"));
        return tasks;
    }

    public Task addTaskToProject(final Long projectId, final Long taskId) {
        final Project project = projectRepository.findById(projectId);
        if (projectId == null) return null;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    public Task removeTaskFromProject(final Long projectId, final Long taskId) {
        if (projectId == null || taskId == null) return null;
        return taskRepository.removeFromProjectByIds(projectId, taskId);
    }

    public Project removeProjectById(final Long projectId) throws ProjectFoundException {
        if (projectId == null) return null;
        for (final Task task : findAllByProjectId(projectId)) {
            taskRepository.removeById(task.getId());
        }
        return projectRepository.removeById(projectId);
    }

    public Project removeProject(final Project project){
        if (project==null) return null;
        try {
            for (final Task task : findAllByProjectId(project.getId())) {
                taskRepository.removeById(task.getId());
            }
        } catch (ProjectFoundException e) {
        }
        return projectRepository.removeProject(project);
    }

    public Project removeProjectByIndex(final int projectIndex) throws ProjectFoundException {
        if (projectIndex > projectRepository.getRepositorySize() - 1 || projectIndex < 0) return null;
        final Long projectId = projectRepository.findByIndex(projectIndex).getId();
        Project project = removeProjectById(projectId);
        if (project == null) throw new ProjectFoundException("Project isn't found by index=".concat(String.valueOf(projectIndex)));
        return project;
    }

    public void clearProjects() {
        projectRepository.clear();
        taskRepository.clear();
    }

    public void removeProjectByUserId(final Long userId) throws ProjectFoundException {
        if (userId == null) return;
        List<Project> projects = projectRepository.findAllByUserId(userId);
        if (projects.isEmpty()) throw new ProjectFoundException("User userId=".concat(" hasn't projects yet."));
        for (Project project : projects) {
            try {
                removeProjectById(project.getId());
            } catch (ProjectFoundException e) {
            }
        }
    }

}
